import "tailwindcss/tailwind.css";
import "./index.css";

export { default as MyButton } from "./lib/Button";
export { default as MyHeader } from "./lib/Header";