import { defineConfig } from "vite";
import * as path from "path";
import react from "@vitejs/plugin-react";
import dts from "vite-plugin-dts";
import checker from "vite-plugin-checker";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    dts({
      insertTypesEntry: true,
    }),
    checker({ typescript: true, enableBuild: true }),
  ],
  build: {
    lib: {
      formats: ["es", "umd"],
      entry: path.resolve(__dirname, "src/index.ts"),
      name: "DesignSystem",
      fileName: (format) => `design-system.${format}.js`,
    },
    rollupOptions: {
      external: ["react", "react-dom", "styled-components"],
      output: {
        globals: {
          react: "React",
          "react-dom": "ReactDOM",
          "styled-components": "styled",
        },
      },
    },
  },
});
