# Boilerplate

## Instal

Due to some peer dependancies not setup well, we need to use this command for the first install

```bash
pnpm install --shamefully-hoist
```

## Setup

To use it, you need to add your conf in some files :

- Your token and your repository id in `'//gitlab.com/api/v4/projects/{your_id_repo}/packages/npm/:_authToken'="my-extremly-secure-token"` in the `.npmrc` file
- Your scope in `@your-scope:registry=https://gitlab.com/api/v4/packages/npm/`in the `.npmrc` file
- `name` and `publishConfig` property in `package.json`

## Use it localy

Start storybook :

```bash
pnpm storybook
```

Get the tgz localy :

```bash
pnpm build-storybook && pnpm pack
```

Import it localy in another project :

```bash
pnpm i path/to/the/build.tgz
```

You should the line like this in your dependancies :

```json
    "@your-scope/design-system": "file:../design-system/design-system-0.0.0.tgz",
```
